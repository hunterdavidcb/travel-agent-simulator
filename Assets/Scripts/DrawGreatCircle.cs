﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawGreatCircle : MonoBehaviour
{
	// Start is called before the first frame update
	SpawnObjectOnGlobe soog;
	int first, second;
	public GameObject plane;
	public GameObject bus;
	public GameObject train;
	public GameObject boat;
	Vector3[] positions;

	public bool setArc = true;

	public delegate void VehicleSoundHandler(int i);
	public event VehicleSoundHandler VehicleSpawned;
	public event VehicleSoundHandler VehicleDestroyed;

	void Start()
	{
		soog = FindObjectOfType<SpawnObjectOnGlobe>();
	}

	// Update is called once per frame
	void Update()
	{
		//if (Input.GetKeyDown(KeyCode.Space))
		//{
		//	GeneratePoints();
		//}

		if (transform.hasChanged)
		{
			UpdatePoints();
		}
	}

	void UpdatePoints()
	{
		positions = new Vector3[30];
		GetComponent<LineRenderer>().positionCount = positions.Length;
		positions[0] = soog.cities[first].position;
		positions[positions.Length - 1] = soog.cities[second].position;

		for (int i = 1; i < positions.Length - 1; i++)
		{
			Vector3 mod = Vector3.Slerp(positions[0], positions[positions.Length - 1], (float)i / positions.Length);
			if (setArc)
			{
				mod += (mod - soog.transform.position).normalized
				* 0.45f * (positions[0] - positions[positions.Length - 1]).magnitude
				* Mathf.Sin((float)i / positions.Length * Mathf.PI);
			}
			positions[i] = mod;
		}

		GetComponent<LineRenderer>().SetPositions(positions);
	}

	public void GeneratePoints(int f, int s)
	{
		first = f;
		second = s;
		

		positions = new Vector3[30];
		GetComponent<LineRenderer>().positionCount = positions.Length;
		positions[0] = soog.cities[first].position;
		positions[positions.Length - 1] = soog.cities[second].position;
		for (int i = 1; i < positions.Length - 1; i++)
		{
			Vector3 mod = Vector3.Slerp(positions[0], positions[positions.Length - 1], (float)i / positions.Length);
			if (setArc)
			{
				mod += (mod - soog.transform.position).normalized
				* 0.45f * (positions[0] - positions[positions.Length - 1]).magnitude
				* Mathf.Sin((float)i / positions.Length * Mathf.PI);
			}
			
			positions[i] = mod;
		}

		GetComponent<LineRenderer>().SetPositions(positions);
		//StartCoroutine(MoveVehicle());

	}

	public void GenerateVehicleMovement(Media media, bool isTrain)
	{
		StartCoroutine(MoveVehicle(media, isTrain));
	}

	IEnumerator MoveVehicle(Media media,bool isTrain)
	{
		GameObject vehicle = new GameObject();
		Destroy(vehicle);
		switch (media)
		{
			case Media.Land:

				vehicle = isTrain ? Instantiate(train, positions[0], Quaternion.identity) :
					Instantiate(bus, positions[0], Quaternion.identity);
				if (VehicleSpawned != null)
				{
					VehicleSpawned(isTrain ? 0 : 1);
				}
				setArc = false;
				break;
			case Media.Water:
				vehicle = Instantiate(boat, positions[0], Quaternion.identity);
				if (VehicleSpawned != null)
				{
					VehicleSpawned(2);
				}
				setArc = false;
				break;
			case Media.Air:
				vehicle = Instantiate(plane, positions[0], Quaternion.identity);
				if (VehicleSpawned != null)
				{
					VehicleSpawned(3);
				}
				setArc = true;
				break;
			default:
				break;
		}
		
		vehicle.transform.SetParent(transform);
		
		//vehicle.transform.LookAt(positions[1]);
		int current = 1;
		vehicle.transform.forward = positions[current] - positions[current - 1];

		float time = 0f;
		float targetTime = 0.25f;
		while (current < positions.Length)
		{
			time += Time.deltaTime;
			//Debug.Log("running");
			vehicle.transform.position = Vector3.Slerp(positions[current - 1], positions[current],
				time/targetTime);



			RaycastHit hit;
			

			if (Physics.Raycast(vehicle.transform.position, (transform.position - vehicle.transform.position).normalized,
				out hit,float.PositiveInfinity,LayerMask.NameToLayer("Globe")))
			{
				vehicle.transform.up = hit.normal * Vector3.Distance(vehicle.transform.position, transform.position);
			}

			vehicle.transform.forward = positions[current] - positions[current - 1];

			if (Vector3.Distance(vehicle.transform.position, positions[current]) < 0.0001f)
			{
				//Debug.Log("updating " + time);
				time = 0f;
				current++;

			}
			yield return null;
		}

		Destroy(vehicle);

		if (VehicleDestroyed != null)
		{
			VehicleDestroyed(0);
		}

		yield return null;
	}
}
