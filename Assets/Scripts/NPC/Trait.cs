﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Trait")]
public class Trait : ScriptableObject
{
	public string description;
	public string[] responses;
	public string[] negResponses;
}
