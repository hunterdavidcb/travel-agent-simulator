﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateNPC : MonoBehaviour
{
	public GameObject npcPrefab;
	public Transform spawnPoint;

	public Location[] startingLocations;

	GameObject currentNPC;
    // Start is called before the first frame update
    void Awake()
    {
		
		FindObjectOfType<GameManager>().NPCReset += Generate;

		if (currentNPC == null)
		{
			Generate();
		}
    }

	void Generate()
	{
		//out with the old and in with the new
		if (currentNPC != null)
		{
			Destroy(currentNPC);
			currentNPC = null;
		}
		
		currentNPC = Instantiate(npcPrefab, spawnPoint);
		NPC npc = new NPC();

		// set personality
		Personality p = new Personality();
		for (int i = 0; i < 5; i++)
		{
			float ran = Random.Range(0f, 33f) + Random.Range(0f, 33f) + Random.Range(0f, 34f);
			p.SetTrait(i, ran);
		}
		npc.SetPersonality(p);

		npc.SetBudget(Random.Range(500f, 5000f));
		npc.SetHometown(startingLocations[Random.Range(0, startingLocations.Length)]);
		currentNPC.GetComponent<NPCHolder>().SetNPC(npc);
		FindObjectOfType<PlayerData>().RegisterNPC(npc);

	}
}
