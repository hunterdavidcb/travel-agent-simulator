﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NPCHolder : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
{
	NPC npc;

	public delegate void NPCClickHandler(NPC npc);
	public event NPCClickHandler NPCClicked;

	public delegate void NPCDestructHandler(NPCHolder npc);
	public event NPCDestructHandler NPCDestroyed;

	public delegate void NPCArrivalHandler(NPC npc);
	public event NPCArrivalHandler NPCArrived;

	Vector3 target;

	public void OnPointerDown(PointerEventData eventData)
	{
		if (NPCClicked != null)
		{
			//Debug.Log("clicked");
			NPCClicked(npc);
		}
	}

	public void SetNPC(NPC n)
	{
		npc = n;
	}
	// Start is called before the first frame update
	void Start()
	{
		target = GameObject.FindGameObjectWithTag("NPCChair").transform.position;
		FindObjectOfType<DialogSystem.DialogBox>().Register(this);
		StartCoroutine(Move());
	}

	// Update is called once per frame
	void Update()
	{
		
	}

	IEnumerator Move()
	{
		Vector3 t = new Vector3(target.x, -.3f,target.z);
		Vector3 p = new Vector3(transform.position.x, -.3f, transform.position.z);
		while (Vector3.Distance(t,p) > .1)
		{

			p = new Vector3(transform.position.x, -.3f, transform.position.z);
			//Debug.Log(Vector3.Distance(t, p));
			transform.Translate((t - p).normalized * Time.deltaTime,Space.World);
			yield return null;
		}

		if (NPCArrived != null)
		{
			NPCArrived(npc);
		}

	}

	void OnDestroy()
	{
		if (NPCDestroyed != null)
		{
			NPCDestroyed(this);
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log("hey");
	}
}
