﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Personality 
{
    [Range(0, 100)]
    float openness;
    [Range(0, 100)]
    float concientiousness;
    [Range(0, 100)]
    float extraversion;
    [Range(0, 100)]
    float agreeableness;
    [Range(0, 100)]
    float neuroticism;

    public Personality()
    {

    }

    public Personality(float o, float c, float e,float a, float n)
    {
        openness = o;
        concientiousness = c;
        extraversion = e;
        agreeableness = a;
        neuroticism = n;
    }

    public float Openness
    {
        get { return openness; }
    }

    public float Conscientiousness
    {
        get { return concientiousness; }
    }

    public float Extraversion
    {
        get { return extraversion; }
    }

    public float Agreeableness
    {
        get { return agreeableness; }
    }

    public float Neuroticism
    {
        get { return neuroticism; }
    }

    public void SetTrait(int index,float amount)
    {
        switch (index)
        {
            case 0:
                openness = amount;
                break;
            case 1:
                concientiousness = amount;
                break;
            case 2:
                extraversion = amount;
                break;
            case 3:
                agreeableness = amount;
                break;
            case 4:
                neuroticism = amount;
                break;
        }
    }

    public float GetTrait(int index)
    {
        switch (index)
        {
            case 0:
                return openness;
            case 1:
                return concientiousness;
            case 2:
                return extraversion;
            case 3:
                return agreeableness;
            case 4:
                return neuroticism;
        }

        return -1;
    }

    public bool CheckMatch(IMatchable matchable)
    {
        float result = matchable.Matches(this);

        return result <= 5f;
    }

    public float PercentMatch(IMatchable matchable)
    {
        return matchable.Matches(this);
    }
}
