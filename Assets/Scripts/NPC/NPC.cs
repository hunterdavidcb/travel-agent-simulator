﻿using DialogSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NPC
{
	private Personality personality;
	private float budget;
	private Location hometown;
	private int preferredLengthOfStay;

	public delegate void NPCReactionHandler(string s);
	public event NPCReactionHandler NPCReacted;

	public delegate void NPCReactionTypeHandler(bool isPositive, Trait t, float amount);
	public event NPCReactionTypeHandler PlayerNPCKnowledgeUpdated;

	//public delegate void NPCAcceptanceHandler(bool accepted, DialogOption DO);
	//public event NPCAcceptanceHandler PlayerNPCAcceptanceUpdated;

	public delegate void NPCNumberHandler(float amount, DialogOption DO);
	public event NPCNumberHandler PlayerNPCNumberUpdated;

	public delegate void NPCEvaluationHandler(float s);
	public event NPCEvaluationHandler NPCEvaluated;

	public Personality Personality
	{
		get { return personality; }
	}

	public float Budget
	{
		get { return budget; }
	}

	public Location Hometown
	{
		get { return hometown; }
	}

	public int LengthOfStay
	{
		get { return preferredLengthOfStay; }
	}

	public void SetPersonality(Personality p)
	{
		personality = p;
	}

	public void SetBudget(float b)
	{
		budget = b;
		//Debug.Log(budget);
		SetLengthOfStay();
	}

	public void SetHometown(Location loc)
	{
		hometown = loc;
	}

	private void SetLengthOfStay()
	{
		if (budget != 0f && personality != null)
		{
			preferredLengthOfStay =
				Mathf.FloorToInt(budget * (Mathf.Log10(budget) *
				Mathf.Sqrt(personality.Openness) / 6100f));
			//Debug.Log(personality.Openness);
			//Debug.Log(preferredLengthOfStay);
		}
	}

	public void ReactTo(Stack<DialogOption> dialogs)
	{
		DialogOption DO = new DialogOption();
		DO.name = "Probe Traits";
		DialogOption bud = new DialogOption();
		bud.name = "Ask about budget";
		DialogOption leng = new DialogOption();
		leng.name = "Ask about length of stay";
		if (dialogs.Contains(DO))
		{
			//Debug.Log("probed");
			if (dialogs.Peek().trait != null)
			{
				if (NPCReacted != null)
				{
					//Debug.Log("here");
					//Debug.Log(GetResponseToTrait(dialogs.Peek().trait));
					NPCReacted(GetResponseToTrait(dialogs.Peek().trait));
				}
			}
		}
		else if (dialogs.Contains(bud))
		{
			if (PlayerNPCNumberUpdated != null)
			{

				PlayerNPCNumberUpdated(budget, bud);
			}

			if (NPCReacted != null)
			{
				//string s = string.Concat("I can spend about ", "\u0024");
				NPCReacted("I can spend about " + string.Format("{0:C}", budget) + ".");
			}
		}
		else if (dialogs.Contains(leng))
		{
			if (PlayerNPCNumberUpdated != null)
			{

				PlayerNPCNumberUpdated(preferredLengthOfStay, leng);
			}

			if (NPCReacted != null)
			{
				NPCReacted("I'd like to stay about " + preferredLengthOfStay.ToString() + " days.");
			}
		}
	}

	private string GetResponseToTrait(Trait t)
	{
		bool re = false;
		switch (t.name)
		{
			case "Openness":
				re = personality.Openness > 51f;
				if (PlayerNPCKnowledgeUpdated != null)
				{
					PlayerNPCKnowledgeUpdated(re, t, personality.Openness);
				}
				return re ? t.responses[UnityEngine.Random.Range(0, t.responses.Length)] :
					t.negResponses[UnityEngine.Random.Range(0, t.negResponses.Length)];
			case "Conscientiousness":
				re = personality.Conscientiousness > 51f;
				if (PlayerNPCKnowledgeUpdated != null)
				{
					PlayerNPCKnowledgeUpdated(re, t, personality.Conscientiousness);
				}
				return re ? t.responses[UnityEngine.Random.Range(0, t.responses.Length)] :
					t.negResponses[UnityEngine.Random.Range(0, t.negResponses.Length)];
			case "Extraversion":
				re = personality.Extraversion > 51f;
				if (PlayerNPCKnowledgeUpdated != null)
				{
					PlayerNPCKnowledgeUpdated(re, t, personality.Extraversion);
				}
				return re ? t.responses[UnityEngine.Random.Range(0, t.responses.Length)] :
					t.negResponses[UnityEngine.Random.Range(0, t.negResponses.Length)];
			case "Agreeableness":
				re = personality.Agreeableness > 51f;
				if (PlayerNPCKnowledgeUpdated != null)
				{
					PlayerNPCKnowledgeUpdated(re, t, personality.Agreeableness);
				}
				return re ? t.responses[UnityEngine.Random.Range(0, t.responses.Length)] :
					t.negResponses[UnityEngine.Random.Range(0, t.negResponses.Length)];
			case "Neuroticism":
				re = personality.Neuroticism > 51f;
				if (PlayerNPCKnowledgeUpdated != null)
				{
					PlayerNPCKnowledgeUpdated(re, t, personality.Neuroticism);
				}
				return re ? t.responses[UnityEngine.Random.Range(0, t.responses.Length)] :
					t.negResponses[UnityEngine.Random.Range(0, t.negResponses.Length)];
		}
		return "";
	}

	public void GetTravelOptionSatisfaction(TravelOption to)
	{
		float sat = 0f;

		//to.availableAccomodation
		//to.location
		//to.possibleActivities
		//to.transportationOption
		//to.availableAccomodation.LengthOfStay
		//to.totalCost

		if (to.possibleActivities.Count > 0)
		{
			float av = 0f;

			for (int i = 0; i < to.possibleActivities.Count; i++)
			{
				float m = personality.PercentMatch(to.possibleActivities[i]);
				Debug.Log("Match " + m);
				if (m < 20)
				{
					m = -((m / 2f) * (m / 2f)) + 100f;
				}
				else
				{
					m = ((m / 4f - 15f) * (m / 4f - 15f)) - 100f;
				}

				av += m;
			}

			av /= to.possibleActivities.Count;
			Debug.Log("Activity sat: " + av);
			sat += av;
		}

		//100f * Mathf.Cos(Mathf.Abs(i - npc.LengthOfStay) / 5f)
		Debug.Log("Length of Stay sat: " + 100f * Mathf.Cos(Mathf.Abs(to.availableAccomodation.LengthOfStay - preferredLengthOfStay) / 5f));
		sat += 100f * Mathf.Cos(Mathf.Abs(to.availableAccomodation.LengthOfStay - preferredLengthOfStay) / 5f);

		if (budget - to.totalCost > 0)
		{
			Debug.Log("Budget sat: " + Mathf.Clamp(((budget - to.totalCost) * (budget - to.totalCost) / budget), -100f, 100f));
			sat += Mathf.Clamp(((budget - to.totalCost) * (budget - to.totalCost) / budget), -100f, 100f);
		}
		else
		{
			Debug.Log("Budget sat: " + Mathf.Clamp(-((budget - to.totalCost) * (budget - to.totalCost) / budget), -100f, 100f));
			sat += Mathf.Clamp(-((budget - to.totalCost) * (budget - to.totalCost) / budget), -100f, 100f);
		}

		Debug.Log("Accommodation Sat: " + (to.availableAccomodation.Stars * personality.Extraversion / 100f) +
			(to.availableAccomodation.Stars * personality.Agreeableness / 100f) +
			-(to.availableAccomodation.Stars * personality.Neuroticism / 100f) +
			Mathf.Clamp(((((to.availableAccomodation.Cost * to.availableAccomodation.LengthOfStay / budget * 100f) + 10000f)) /
			(to.availableAccomodation.Cost * to.availableAccomodation.LengthOfStay / budget * 100f)) - 200f, -100f, 100f));

		sat += (to.availableAccomodation.Stars * personality.Extraversion / 100f) +
			(to.availableAccomodation.Stars * personality.Agreeableness / 100f) +
			-(to.availableAccomodation.Stars * personality.Neuroticism / 100f) +
			Mathf.Clamp(((((to.availableAccomodation.Cost * to.availableAccomodation.LengthOfStay / budget * 100f) + 10000f)) /
			(to.availableAccomodation.Cost * to.availableAccomodation.LengthOfStay / budget * 100f)) - 200f, -100f, 100f);

		float desiredDistance = ((personality.Openness - 30f) * (personality.Openness - 30f) / 70f) * 20037f;
		float diff = to.location.Distance(hometown) - desiredDistance;


		Debug.Log("Distance Sat: " + 100f * Mathf.Cos(diff / 5037f));
		sat += 100f * Mathf.Cos(diff / 5037f);//(personality.Openness * personality.Openness) * Mathf.Log(to.location.Distance(hometown) / 21781f);

		sat /= 5f;

		if (NPCEvaluated != null)
		{
			NPCEvaluated(sat);
		}
		Debug.Log("Total sat: " + sat);
		//return sat;
	}
}