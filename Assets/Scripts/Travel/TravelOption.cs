﻿using System;
using System.Collections.Generic;

[Serializable]
public class TravelOption
{
	public float totalCost;
	public Location location;
	public List<Activity> possibleActivities;
	public AccomodationData availableAccomodation;
	public TransportationData transportationOption;

	public TravelOption()
	{

	}

	public TravelOption(TravelOption to)
	{
		totalCost = to.totalCost;
		location = to.location;
		possibleActivities = to.possibleActivities;
		availableAccomodation = to.availableAccomodation;
		transportationOption = to.transportationOption;
	}
}
