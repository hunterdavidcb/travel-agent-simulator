﻿using System;
[Serializable]
public class TransportationData : ITransportation
{
	public float Cost
	{
		get { return cost; }
	}

	public float Speed
	{
		get { return speed; }
	}

	public Media Mode
	{
		get { return mode; }
	}

	public Class ClassType
	{
		get { return classType; }
	}

	public string Name
	{
		get { return name; }
	}

	private string name;
	private float cost;
	private float speed;
	private Media mode;
	private Class classType;

	public void SetClass(Class c)
	{
		classType = c;
	}

	public void SetCost(float f)
	{
		cost = f;
	}

	public void SetMedia(Media m)
	{
		mode = m;
	}

	public void SetSpeed(float s)
	{
		speed = s;
	}

	public void SetName(string n)
	{
		name = n;
	}
}