﻿
public interface ITransportation
{
	string Name { get; }
	float Cost { get; }
	float Speed { get; }
	Media Mode { get; }
	Class ClassType { get; }

	void SetName(string n);
	void SetCost(float f);
	void SetSpeed(float s);
	void SetMedia(Media m);
	void SetClass(Class c);
}