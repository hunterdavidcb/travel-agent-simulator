﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Transporation")]
public class Transportation : ScriptableObject, ITransportation
{
	[Tooltip("kph")]
	public float speed;
	[Tooltip("per km")]
	public float cost; //per 100 km
	public Media mode;
	public Class Class;

	public float Cost
	{
		get { return cost; }
	}

	public float Speed
	{
		get { return speed; }
	}

	public Media Mode
	{
		get { return mode; }
	}

	public Class ClassType
	{
		get { return Class; }
	}

	public string Name
	{
		get { return name; }
	}

	public void SetClass(Class c)
	{
		Class = c;
	}

	public void SetCost(float f)
	{
		cost = f;
	}

	public void SetMedia(Media m)
	{
		mode = m;
	}

	public void SetName(string n)
	{
		name = n;
	}

	public void SetSpeed(float s)
	{
		speed = s;
	}
}

public enum Media
{
	Land,
	Water,
	Air
}

public enum Class
{
	Economy,
	Business,
	First
}
