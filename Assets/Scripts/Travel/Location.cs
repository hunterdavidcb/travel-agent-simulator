﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Location")]
public class Location : ScriptableObject, IEquatable<Location>
{
	public LatLong latLong;
	public List<LocationTags> tags = new List<LocationTags>();
	public LandType landType;
	public Continent continent;
	public Sprite sprite;

	public bool Equals(Location other)
	{
		int allSame = 0;
		for (int i = 0; i < tags.Count; i++)
		{
			for (int x = 0; x < other.tags.Count; x++)
			{
				if (tags[i].Equals(other.tags[x]))
				{
					allSame++;
				}
			}
		}
		return name.Equals(other.name);// && landType.Equals(other.landType) && continent.Equals(other.continent) && allSame == tags.Count;
	}

	public float Distance(Location other)
	{
		float dist = 0f;

		float radianLat = latLong.latitude * Mathf.Deg2Rad;
		float radianLong = latLong.longitude * Mathf.Deg2Rad;

		float radianLatOther = other.latLong.latitude * Mathf.Deg2Rad;
		float radianLongOther = other.latLong.longitude * Mathf.Deg2Rad;

		float sinPart = Mathf.Sin(radianLat) * Mathf.Sin(radianLatOther);
		float cosPart = Mathf.Cos(radianLat) * Mathf.Cos(radianLatOther)
			* Mathf.Cos(Mathf.Abs(radianLong - radianLongOther));
		float centralAngle = Mathf.Acos(sinPart + cosPart);
		const float earthRadius = 6371f;

		dist = centralAngle * earthRadius;

		return dist;
	}
}

public enum LocationTags
{
	City,
	Seaside,
	Mountainous,
	Jungle,
	Lake,
	Desert
}

public enum LandType
{
	Continental,
	Island
}

public enum Continent
{
	NorthAmerica,
	SouthAmerica,
	Australia,
	Eurasia,
	Africa
}

[Serializable]
public struct LatLong
{
	public float latitude;
	public float longitude;
}
