﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelOptionsHolder : MonoBehaviour
{
	public Transportation[] transportOptions;
	public Accomodation[] accomodations;
	public Activity[] activities;
	public Location[] locations;

	Dictionary<Location, Dictionary<AccomodationData,TravelOption>> travelOptions = 
		new Dictionary<Location, Dictionary<AccomodationData, TravelOption>>();

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		//if (Input.GetKeyDown(KeyCode.T))
		//{
		//	travelOptions.Clear();
		//	GenerateOptions();
		//	TestGeneration();
		//}

		//if (Input.GetKeyDown(KeyCode.D))
		//{
		//	TestDistanceFormula();
		//}
	}

	void GenerateOptions()
	{
		Location origin = locations[Random.Range(0, locations.Length)];
		Debug.Log("Starting at: " + origin.name);
		foreach (Location loc in locations)
		{
			if (loc.Equals(origin))
			{
				continue;
			}

			travelOptions.Add(loc, new Dictionary<AccomodationData, TravelOption>());

			//generate transportation options
			if (origin.landType == LandType.Island)
			{
				foreach(Transportation trans in transportOptions)
				{
					if (trans.mode == Media.Air)
					{
						TravelOption to = new TravelOption();
						to.location = loc;
						to.transportationOption = new TransportationData();
						to.transportationOption.SetName(trans.name);
						to.transportationOption.SetSpeed(trans.Speed);
						to.transportationOption.SetMedia(trans.Mode);
						to.transportationOption.SetClass(trans.ClassType);
						to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
						//take care of accomodation
						//travelOptions[loc].Add(to);
						Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
						foreach (var item in dic.Keys)
						{
							travelOptions[loc].Add(item, dic[item]);
						}
						//travelOptions[loc].Add()
					}
					else if (trans.mode == Media.Water)
					{
						if ((loc.tags.Contains(LocationTags.Lake) ||
						loc.tags.Contains(LocationTags.Seaside)) &&
						(origin.tags.Contains(LocationTags.Lake) ||
						origin.tags.Contains(LocationTags.Seaside)))
						{
							TravelOption to = new TravelOption();
							to.location = loc;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(trans.name);
							to.transportationOption.SetSpeed(trans.Speed);
							to.transportationOption.SetMedia(trans.Mode);
							to.transportationOption.SetClass(trans.ClassType);
							to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
							//take care of accomodation
							//travelOptions[loc].Add(to);
							Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
							foreach (var item in dic.Keys)
							{
								travelOptions[loc].Add(item, dic[item]);
							}
						}
					}
				}
			}
			//it is continental
			else
			{
				foreach (Transportation trans in transportOptions)
				{
					if (origin.continent.Equals(loc.continent))
					{
						//if the continent is the same, allow ground travel
						if (trans.mode == Media.Land)
						{
							TravelOption to = new TravelOption();
							to.location = loc;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(trans.name);
							to.transportationOption.SetSpeed(trans.Speed);
							to.transportationOption.SetMedia(trans.Mode);
							to.transportationOption.SetClass(trans.ClassType);
							to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
							//take care of accomodation
							//travelOptions[loc].Add(to);
							Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
							foreach (var item in dic.Keys)
							{
								travelOptions[loc].Add(item, dic[item]);
							}
						}
						else if (trans.mode == Media.Water)
						{
							//water options are available
							if (loc.tags.Contains(LocationTags.Lake) &&
							origin.tags.Contains(LocationTags.Lake))
							{

								

							}
						}
						else
						{
							TravelOption to = new TravelOption();
							to.location = loc;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(trans.name);
							to.transportationOption.SetSpeed(trans.Speed);
							to.transportationOption.SetMedia(trans.Mode);
							to.transportationOption.SetClass(trans.ClassType);
							to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
							//take care of accomodation
							//travelOptions[loc].Add(to);
							Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
							foreach (var item in dic.Keys)
							{
								travelOptions[loc].Add(item, dic[item]);
							}
						}
					}
					//the continents are not the same
					else
					{
						if (trans.mode == Media.Water)
						{
							//water options are available
							if ((loc.tags.Contains(LocationTags.Lake) ||
								loc.tags.Contains(LocationTags.Seaside)) &&
								(origin.tags.Contains(LocationTags.Lake) ||
								origin.tags.Contains(LocationTags.Seaside)))
							{

								TravelOption to = new TravelOption();
								to.location = loc;
								to.transportationOption = new TransportationData();
								to.transportationOption.SetName(trans.name);
								to.transportationOption.SetSpeed(trans.Speed);
								to.transportationOption.SetMedia(trans.Mode);
								to.transportationOption.SetClass(trans.ClassType);
								to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
								//take care of accomodation
								//travelOptions[loc].Add(to);
								Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
								foreach (var item in dic.Keys)
								{
									travelOptions[loc].Add(item, dic[item]);
								}

							}
						}
						else if (trans.mode == Media.Air)
						{
							TravelOption to = new TravelOption();
							to.location = loc;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(trans.name);
							to.transportationOption.SetSpeed(trans.Speed);
							to.transportationOption.SetMedia(trans.Mode);
							to.transportationOption.SetClass(trans.ClassType);
							to.transportationOption.SetCost(origin.Distance(loc) * trans.Cost);
							//take care of accomodation
							//travelOptions[loc].Add(to);
							Dictionary<AccomodationData, TravelOption> dic = GetAccomodations(to);
							foreach (var item in dic.Keys)
							{
								travelOptions[loc].Add(item, dic[item]);
							}
						}
					}

					
				}
				
			}
		}


	}

	Dictionary<AccomodationData,TravelOption> GetAccomodations(TravelOption to)
	{
		Dictionary<AccomodationData, TravelOption> dic = 
			new Dictionary<AccomodationData, TravelOption>();
		
		foreach (var ac in accomodations)
		{
			TravelOption copy = new TravelOption(to);
			copy.availableAccomodation = new AccomodationData(ac.Name,ac.costPerNight,ac.Stars,0,ac.RoomType);
			dic.Add(copy.availableAccomodation, copy);
		}

		return dic;
	}

	void TestDistanceFormula()
	{
		foreach (Location loc in locations)
		{
			foreach (Location locPrime in locations)
			{
				if (loc.Equals(locPrime))
				{
					continue;
				}

				Debug.Log("Distance from " + loc.name + " to " + locPrime.name + ": " 
					+ loc.Distance(locPrime));
			}
		}
	}

	void TestGeneration()
	{
		Debug.Log("here");
		foreach (Location loc in travelOptions.Keys)
		{
			foreach (AccomodationData acc in travelOptions[loc].Keys)
			{
				Debug.Log("Location: " + loc.name + ", Accomodation: " + acc.Name + " " + travelOptions[loc][acc].transportationOption.Name);
			}
		}
	}

}
