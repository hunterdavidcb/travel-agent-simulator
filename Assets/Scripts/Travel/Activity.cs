﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Activity")]
public class Activity : ScriptableObject, IMatchable
{
	[Tooltip("in minutes")]
	public float timeToDo; //in minutes
	[Tooltip("in dollars")]
	public float cost;

	public List<LocationTags> tags = new List<LocationTags>();

	[Range(0f,100f)]
	public float openness;
	[Range(0f, 100f)]
	public float conscientiousness;
	[Range(0f, 100f)]
	public float extraversion;
	[Range(0f, 100f)]
	public float agreeableness;
	[Range(0f, 100f)]
	public float neuroticism;

	public string Name
	{
		get { return name; }
	}

	public float Matches(Personality p)
	{
		float av = 0f;

		av += Mathf.Abs(openness - p.Openness);
		av += Mathf.Abs(conscientiousness - p.Conscientiousness);
		av += Mathf.Abs(extraversion - p.Extraversion);
		av += Mathf.Abs(agreeableness - p.Agreeableness);
		av += Mathf.Abs(neuroticism - p.Neuroticism);

		av /= 5f;

		return av;
	}
}

public enum Season
{
	Spring,
	Summer,
	Fall,
	Winter
}
