﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Accomadation")]
public class Accomodation : ScriptableObject, IAccomodation
{
	[Tooltip("in dollars")]
	public float costPerNight;
	public RoomType roomType;
	[Range(0,5)]
	public int stars;

	public float Cost
	{
		get { return costPerNight; }
	}

	public RoomType RoomType
	{
		get { return roomType; }
	}

	public int Stars
	{
		get { return stars; }
	}

	public string Name
	{
		get { return name; }
	}
}

public enum RoomType
{
	Single,
	Double,
	King,
	Suite,
	Penthouse,
	Villa
}
