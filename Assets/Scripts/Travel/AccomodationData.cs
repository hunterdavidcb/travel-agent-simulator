﻿using System;
[Serializable]
public class AccomodationData: IAccomodation
{
	private int lengthOfStay;
	private float costPerNight;
	private RoomType roomType;
	private int stars;
	private string name;

	public float Cost
	{
		get { return costPerNight; }
	}

	public float TotalCost
	{
		get { return lengthOfStay* costPerNight; }
	}

	public RoomType RoomType
	{
		get { return roomType; }
	}

	public int Stars
	{
		get { return stars; }
	}

	public int LengthOfStay
	{
		get { return lengthOfStay; }
	}

	public string Name
	{
		get { return name; }
	}

	public AccomodationData(string n, float c, int s, int stay, RoomType r)
	{
		name = n;
		costPerNight = c;
		stars = s;
		lengthOfStay = stars;
		roomType = r;
	}


}