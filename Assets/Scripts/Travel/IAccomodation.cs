﻿public interface IAccomodation
{
	float Cost { get; }
	RoomType RoomType { get; }

	int Stars { get; }
	string Name { get; }
}