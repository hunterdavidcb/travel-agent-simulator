﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowComputerTooltip : MonoBehaviour
{
	public RectTransform tooltip;


	void Awake()
	{ 

		tooltip.gameObject.SetActive(false);

		//Tooltip[] tips = FindObjectsOfType<Tooltip>();
		////go through and subscribe;
		//for (int i = 0; i < tips.Length; i++)
		//{
		//	tips[i].ShowTip += OnShowTip;
		//	tips[i].HideTip += OnHideTip;
		//}
	}

	public void OnHideTip(string m, Vector3 pos)
	{
		tooltip.anchoredPosition = Vector3.zero;
		tooltip.gameObject.SetActive(false);
	}

	public void OnShowTip(string m, Vector3 pos)
	{

		//pos = Camera.main.WorldToScreenPoint(pos);
		//Debug.Log(pos);
		
		//pos.x -= 40;
		pos.z = 0f;
		//tooltip.position = pos;
		Vector2 p;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), pos, Camera.main, out p);
		//Debug.Log(p);
		p.y -= 45;
		tooltip.anchoredPosition = p;
		tooltip.GetComponentInChildren<Text>().text = m;
		tooltip.gameObject.SetActive(true);

	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
