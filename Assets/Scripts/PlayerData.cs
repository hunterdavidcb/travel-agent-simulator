﻿using System;
using System.Collections;
using System.Collections.Generic;
using DialogSystem;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{
	//keep track of the questions asked, the npcs helped, and the info about each npc:
	// how happy they are with the destination, accomodation, budget, activities, etc
	Dictionary<string, GameObject> QandA =
		new Dictionary<string, GameObject>();

	public GameObject knowledgeItemPrefab;
	public GameObject infoPanel;
	public GameObject customerSatisfactionObject;
	public Sprite money;
	public Sprite con;
	public Sprite agree;
	public Sprite open;
	public Sprite neur;
	public Sprite extra;
	public Sprite length;
	public Sprite hometown;

	int numCustomers;
	float customerSatisfaction;
	Location suggestedDestination;
	Accomodation suggestedAccomodation;
	Transportation suggestedTransportation;
	int suggestedLengthOfStay;
	float currentBudget;
	List<Activity> suggestedActivities = new List<Activity>();

	//this one is not necessary. probably.
	TravelOptionsHolder generate;
	public DrawGreatCircle dgc;
	private NPC npc;

	public delegate void BudgetHandler(float delta);
	public event BudgetHandler BudgetChanged;

	public delegate void TravelOptionSubmissionHandler(TravelOption to);
	public event TravelOptionSubmissionHandler TravelOptionSubmitted;

	public delegate void Finisher();
	public event Finisher Finished;

	public Location SuggestedDestination
	{
		get { return suggestedDestination; }
	}

	public Location NPCHometown
	{
		get { return npc.Hometown; }
	}

	public float CurrentBudget
	{
		get { return currentBudget; }
	}


	private void Awake()
	{
		generate = GetComponent<TravelOptionsHolder>();
		GetComponent<ComputerControler>().Submitted += OnSubmission;
	}

	private void OnSubmission()
	{
		//throw new NotImplementedException();
		TravelOption to = new TravelOption();
		AccomodationData accomodationData = new AccomodationData(suggestedAccomodation.Name,
			suggestedAccomodation.Cost, suggestedAccomodation.Stars, suggestedLengthOfStay,
			suggestedAccomodation.RoomType);
		to.availableAccomodation = accomodationData;
		to.location = suggestedDestination;
		TransportationData ta = new TransportationData();
		ta.SetMedia(suggestedTransportation.Mode);
		ta.SetName(suggestedTransportation.Name);
		ta.SetSpeed(suggestedTransportation.Speed);
		ta.SetName(suggestedTransportation.Name);
		ta.SetCost(suggestedTransportation.Cost * suggestedDestination.Distance(npc.Hometown));
		to.transportationOption = ta;

		to.possibleActivities = suggestedActivities;

		to.totalCost = 0f;
		to.totalCost += ta.Cost;
		to.totalCost += accomodationData.Cost;
		for (int i = 0; i < to.possibleActivities.Count; i++)
		{
			to.totalCost += to.possibleActivities[i].cost;
		}

		if (TravelOptionSubmitted != null)
		{
			TravelOptionSubmitted(to);
		}
	}

	protected void ONNPCEvaluationFinished(float s)
	{

		customerSatisfaction += s;
		numCustomers++;

		customerSatisfactionObject.GetComponent<Tooltip>().message =
			"Custom Satisfaction: " + Mathf.FloorToInt(customerSatisfaction/numCustomers).ToString();

		int first = -1;
		int second = -1;

		//get the indices for the hometown and destination
		for (int i = 0; i < generate.locations.Length; i++)
		{
			if (generate.locations[i].name.Equals(npc.Hometown.name))
			{
				first = i;
			}
			else if (generate.locations[i].name.Equals(suggestedDestination.name))
			{
				second = i;
			}
		}

		dgc.GeneratePoints(first,second);
		dgc.GenerateVehicleMovement(suggestedTransportation.mode,
			suggestedTransportation.name.Contains("Train"));

		suggestedAccomodation = null;
		suggestedActivities = null;
		suggestedDestination = null;
		suggestedLengthOfStay = 0;
		suggestedTransportation = null;
		npc = null;
		

		if (Finished != null)
		{
			Finished();
		}
	}

	public void RegisterNPC(NPC npc)
	{
		foreach (var s in QandA.Keys)
		{
			Destroy(QandA[s]);
		}
		QandA.Clear();

		this.npc = npc;
		npc.PlayerNPCKnowledgeUpdated += OnNPCKNowledgeUpdated;
		//npc.PlayerNPCAcceptanceUpdated += OnNPCAcceptanceUpdated;
		npc.PlayerNPCNumberUpdated += OnNPCNumberUpdated;
		npc.NPCEvaluated += ONNPCEvaluationFinished;

		GameObject go = Instantiate(knowledgeItemPrefab, infoPanel.transform);
		go.GetComponent<Knowledge>().baseImage.sprite = hometown;
		go.GetComponent<Tooltip>().message = "Hometown: " + npc.Hometown.name;

		QandA.Add(npc.Hometown.name, go);
		TravelOptionSubmitted += npc.GetTravelOptionSatisfaction;

		//testing
		//TestOpenness();
		//TestLength();
		//TestActivities();
		//TestAccomodation();
	}

	void TestOpenness()
	{
		Debug.Log(npc.Personality.Openness);
		Debug.Log(npc.Hometown.name);
		float desiredDistance = ((npc.Personality.Openness - 30f) * (npc.Personality.Openness - 30f) / 70f) * 20037;
		Debug.Log("Desired Distance: " + desiredDistance);
		for (int i = 0; i < generate.locations.Length; i++)
		{
			Debug.Log("Destination: " + generate.locations[i].name + "("
				+ generate.locations[i].Distance(npc.Hometown).ToString() + ")");
			float diff = generate.locations[i].Distance(npc.Hometown) - desiredDistance;


			Debug.Log(100f * Mathf.Cos(diff / 5037f));
		}
	}

	void TestLength()
	{
		Debug.Log(npc.Personality.Conscientiousness);
		Debug.Log(npc.LengthOfStay);
		int i = Mathf.Max(1, npc.LengthOfStay - 10);
		int max = Mathf.Min(30, npc.LengthOfStay + 10);
		for (; i < max; i++)
		{
			Debug.Log(100f * Mathf.Cos(Mathf.Abs(i - npc.LengthOfStay) / 5f));
		}
	}

	void TestActivities()
	{
		Debug.Log("O " + npc.Personality.Openness);
		Debug.Log("C " + npc.Personality.Conscientiousness);
		Debug.Log("E " + npc.Personality.Extraversion);
		Debug.Log("A " + npc.Personality.Agreeableness);
		Debug.Log("N " + npc.Personality.Neuroticism);

		for (int i = 0; i < generate.activities.Length; i++)
		{
			//Debug.Log("O " + generate.activities[i].openness);
			//Debug.Log("C " + generate.activities[i].conscientiousness);
			//Debug.Log("E " + generate.activities[i].extraversion);
			//Debug.Log("A " + generate.activities[i].agreeableness);
			//Debug.Log("N " + generate.activities[i].neuroticism);
			float m = generate.activities[i].Matches(npc.Personality);
			Debug.Log("Match " + m);
			float sat;
			if (m < 20)
			{
				sat = -((m / 2f) * (m / 2f)) + 100f;
			}
			else
			{
				sat = ((m / 4f - 15f) * (m / 4f - 15f)) - 100f;
			}
			Debug.Log("Sat: " + sat);
		}
	}

	void TestAccomodation()
	{
		//extraversion and agreeableness increase satisfaction
		//neuroticism decreases satisfaction
		//Personality, satisfation, image, ambience, and loyality
		//by Dev Jani, Heesup Han
		//Interantional Journal of  Hospitality Management 37 (2014) 11-20
		for (int i = 0; i < generate.accomodations.Length; i++)
		{
			int s = Mathf.Max(1, npc.LengthOfStay - 10);
			int max = Mathf.Min(30, npc.LengthOfStay + 10);
			for (; s < max; s++)
			{
				Debug.Log("Stay: " + s);
				float sat = (generate.accomodations[i].Stars * npc.Personality.Extraversion / 100f) +
			(generate.accomodations[i].Stars * npc.Personality.Agreeableness / 100f) +
			-(generate.accomodations[i].Stars * npc.Personality.Neuroticism / 100f) +
			Mathf.Clamp(((((generate.accomodations[i].Cost * s / npc.Budget * 100f) + 10000f)) /
			(generate.accomodations[i].Cost * s / npc.Budget * 100f)) - 200f,-100f,100f);
				Debug.Log("Stars: " + generate.accomodations[i].Stars);
				Debug.Log("Budget: " + npc.Budget);
				Debug.Log("Used: " + generate.accomodations[i].Cost * s);
				Debug.Log("Sat: " + sat);
			}
		}
	}

	private void OnNPCNumberUpdated(float amount, DialogOption DO)
	{
		if (DO.name.Equals("Ask about budget"))
		{
			currentBudget = amount;
			if (!QandA.ContainsKey(DO.name))
			{
				GameObject go = Instantiate(knowledgeItemPrefab, infoPanel.transform);
				go.GetComponent<Knowledge>().baseImage.sprite = money;
				//string s = string.Concat("Budget: ", "\u0024");
				go.GetComponent<Tooltip>().message = "Budget: " + string.Format("{0:C}",amount);
				QandA.Add(DO.name, go);
			}
			
		}
		else if (DO.name.Equals("Ask about length of stay"))
		{
			suggestedLengthOfStay = Mathf.FloorToInt(amount);
			if (!QandA.ContainsKey(DO.name))
			{
				GameObject go = Instantiate(knowledgeItemPrefab, infoPanel.transform);
				go.GetComponent<Knowledge>().baseImage.sprite = length;
				go.GetComponent<Tooltip>().message = "Preferred Length of Stay: " + amount.ToString() + " day(s)";
				QandA.Add(DO.name, go);
			}
		}
	}

	//private void OnNPCAcceptanceUpdated(bool accepted, DialogOption DO)
	//{
	//	switch (DO.name)
	//	{
	//		case "Suggest destination":
	//			// use DO.say to store the name of the destination
	//			break;
	//		case "Suggest accomodation":
	//			// use DO.say to store the name of the accomodation
	//			break;
	//		case "Suggest activity":
	//			// use DO.say to store the name of the activity
	//			break;
	//		default:
	//			break;
	//	}
	//}

	protected void OnNPCKNowledgeUpdated(bool isPositive,Trait t,float amount)
	{
		//make sure that we don't already contain the trait

		string DO = "Probe " + t.name;
		if (!QandA.ContainsKey(DO))
		{
			GameObject go = Instantiate(knowledgeItemPrefab, infoPanel.transform);
			//set the sprite to appropirate image
			switch (t.name)
			{
				case "Openness":
					go.GetComponent<Knowledge>().baseImage.sprite = open;
					go.GetComponent<Tooltip>().message = "Openness: " + string.Format("{0:00}", amount) +
						"\n" + t.description;
					break;
				case "Conscientiousness":
					go.GetComponent<Knowledge>().baseImage.sprite = con;
					go.GetComponent<Tooltip>().message = "Conscientiousness: " + string.Format("{0:00}", amount) +
						"\n" + t.description;
					break;
				case "Extraversion":
					go.transform.GetChild(0).GetComponent<Image>().sprite = extra;
					go.GetComponent<Tooltip>().message = "Extraversion: " + string.Format("{0:00}", amount) +
						"\n" + t.description;
					break;
				case "Agreeableness":
					go.GetComponent<Knowledge>().baseImage.sprite = agree;
					go.GetComponent<Tooltip>().message = "Agreeableness: " + string.Format("{0:00}", amount) +
						"\n" + t.description;
					break;
				case "Neuroticism":
					go.GetComponent<Knowledge>().baseImage.sprite = neur;
					go.GetComponent<Tooltip>().message = "Neuroticism: " + string.Format("{0:00}", amount) +
						"\n" + t.description;
					break;
			}

			go.GetComponent<Knowledge>().fillImage.fillAmount = amount / 100f;
			QandA.Add(DO, go);
			//Debug.Log("added");
		}
	}

	public void SetDestination(Location loc)
	{
		suggestedDestination = loc;
	}

	public void SetAccomodation(Accomodation acc)
	{
		if (suggestedAccomodation != acc)
		{
			if (BudgetChanged != null)
			{
				float d;
				if (suggestedAccomodation == null)
				{
					if (acc == null)
					{
						d = 0f;
					}
					else
					{
						d = acc.Cost * suggestedLengthOfStay;
					}
				}
				else
				{
					d = (acc.Cost - suggestedAccomodation.Cost) * suggestedLengthOfStay;
				}
				currentBudget += d;
				BudgetChanged(d);
			}
		}
		suggestedAccomodation = acc;
	}

	public void SetTransportation(Transportation transportation)
	{
		if (suggestedTransportation != transportation)
		{
			if (BudgetChanged != null)
			{
				float d;
				if (suggestedTransportation == null)
				{
					if (transportation == null)
					{
						d = 0f;
					}
					else
					{
						d = transportation.Cost * npc.Hometown.Distance(suggestedDestination) / 100f;
					}
				}
				else
				{
					d = (transportation.Cost - suggestedTransportation.Cost) * npc.Hometown.Distance(suggestedDestination)/100f;
				}
				currentBudget += d;
				BudgetChanged(d);
			}
		}
		suggestedTransportation = transportation;
	}

	public void AddActivity(Activity act)
	{
		suggestedActivities.Add(act);

		if (BudgetChanged != null)
		{
			BudgetChanged(act.cost);
		}
		currentBudget += act.cost;
	}

	public void RemoveActivity(Activity act)
	{
		suggestedActivities.Remove(act);

		if (BudgetChanged != null)
		{
			BudgetChanged(-act.cost);
		}
		currentBudget -= act.cost;
	}

	public void SetLength(int length)
	{
		int del = length - suggestedLengthOfStay;
		//Debug.Log(length);
		//Debug.Log(suggestedLengthOfStay);
		suggestedLengthOfStay = length;
		if (suggestedAccomodation != null)
		{
			currentBudget += del * suggestedAccomodation.Cost;
			if (BudgetChanged != null)
			{
				BudgetChanged(del * suggestedAccomodation.Cost);
			}
		}
	}
}
