﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMatchable
{
    string Name { get; }
    float Matches(Personality p);
}
