﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogProbe")]
public class DialogProbe : Dialog
{
	public Trait trait;
}
