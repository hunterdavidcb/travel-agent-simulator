﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Dialog:ScriptableObject
{
	public string say;
	public List<Dialog> subOptions = new List<Dialog>();
}
