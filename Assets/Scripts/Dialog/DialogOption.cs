﻿using System;
using System.Collections.Generic;

namespace DialogSystem
{
	//[Serializable]
	public class DialogOption : IEquatable<DialogOption>
	{
		public string name;
		public string say;
		public Trait trait;
		//public Location location;
		//public Accomodation accomodation;
		//public Activity activity;
		public List<DialogOption> subOptions = new List<DialogOption>();

		public bool Equals(DialogOption other)
		{
			//Debug.Log(name.Equals(other.name));
			return name.Equals(other.name);
		}

		public void SetSuboptions(List<Dialog> dialogs)
		{
			for (int i = 0; i < dialogs.Count; i++)
			{
				DialogOption dialogOption = new DialogOption();
				dialogOption.name = dialogs[i].name;
				dialogOption.say = dialogs[i].say;
				if (dialogs[i] is DialogProbe)
				{
					dialogOption.trait = ((DialogProbe)dialogs[i]).trait;
				}
				//else if (dialogs[i] is DialogAccomodation)
				//{
				//	dialogOption.accomodation = ((DialogAccomodation)dialogs[i]).accomodation;
				//}
				//else if (dialogs[i] is DialogActivity)
				//{
				//	dialogOption.activity = ((DialogActivity)dialogs[i]).activity;
				//}
				//else if (dialogs[i] is DialogDestination)
				//{
				//	dialogOption.location = ((DialogDestination)dialogs[i]).location;
				//}
				dialogOption.SetSuboptions(dialogs[i].subOptions);
				subOptions.Add(dialogOption);
			}
		}
	}
}