﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTooltip : MonoBehaviour
{
	RectTransform tooltip;


    void Awake()
	{
		tooltip = transform.GetChild(3).GetComponent<RectTransform>();

		tooltip.gameObject.SetActive(false);

		//Tooltip[] tips = FindObjectsOfType<Tooltip>();
		////go through and subscribe;
		//for (int i = 0; i < tips.Length; i++)
		//{
		//	tips[i].ShowTip += OnShowTip;
		//	tips[i].HideTip += OnHideTip;
		//}
	}

	public void OnHideTip(string m,Vector3 pos)
	{
		tooltip.anchoredPosition = Vector3.zero;
		tooltip.gameObject.SetActive(false);
	}

	public void OnShowTip(string m,Vector3 pos)
	{

		//pos = Camera.main.WorldToScreenPoint(pos);
		//Debug.Log(pos);
		pos.y -= 45;
		pos.x -= 40;
		pos.z = 0f;
		//tooltip.position = pos;
		tooltip.anchoredPosition = pos;
		tooltip.GetComponentInChildren<Text>().text = m;
		tooltip.gameObject.SetActive(true);

	}


	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
