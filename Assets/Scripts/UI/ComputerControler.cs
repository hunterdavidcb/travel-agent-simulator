﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ComputerControler : MonoBehaviour
{
	public GameObject openComputer;
	public GameObject closedComputer;
	public GameObject infoPrefab;
	public GameObject infoBudgetPrefab;
	public GameObject budgetPopup;
	bool open = false;


	public GameObject displayPanel;
	public GameObject horizontalBar;
	public GameObject slider;

	public GameObject destButton;
	public GameObject transButton;
	public GameObject accomButton;
	public GameObject lengthButton;
	public GameObject actButton;
	public GameObject subButton;

	TravelOptionsHolder generator;
	PlayerData playerData;

	GameObject destinationInfo;
	GameObject transportationInfo;
	GameObject budgetInfo;
	GameObject accomodationInfo;
	GameObject lengthOfStayInfo;
	public Dictionary<string,GameObject> activityInfo = new Dictionary<string, GameObject>();

	//Location npcHometown;
	//Location destination;

	public ShowComputerTooltip showTooltip; //for the computer only

	List<GameObject> options = new List<GameObject>();

	public delegate void SubmissionHandler();
	public event SubmissionHandler Submitted;
	// Start is called before the first frame update
	void Start()
	{
		openComputer.SetActive(false);
		closedComputer.SetActive(true);
		slider.SetActive(false);
		generator = GetComponent<TravelOptionsHolder>();
		playerData = GetComponent<PlayerData>();
		playerData.BudgetChanged += OnBudgetChanged;
		playerData.Finished += OnCustomerFinished;
		SetButtons(0);

	}

	private void OnCustomerFinished()
	{
		ClearOptions();
		openComputer.SetActive(false);
		closedComputer.SetActive(true);
		slider.SetActive(false);
		SetButtons(0);
	}

	//public void SetHometown(Location l)
	//{
	//	//Debug.Log(l.name);
	//	npcHometown = l;
	//}

	void SetButtons(int index)
	{
		switch (index)
		{
			case 0:
				destButton.SetActive(true);
				transButton.SetActive(false);
				accomButton.SetActive(false);
				lengthButton.SetActive(false);
				actButton.SetActive(false);
				subButton.SetActive(false);
				break;
			case 1:
				transButton.SetActive(true);
				accomButton.SetActive(false);
				lengthButton.SetActive(false);
				actButton.SetActive(false);
				subButton.SetActive(false);
				break;
			case 2:
				accomButton.SetActive(true);
				lengthButton.SetActive(true);
				actButton.SetActive(false);
				subButton.SetActive(false);
				break;
			case 3:
				actButton.SetActive(true);
				subButton.SetActive(true);
				break;
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.C))
		{
			SwitchComputers();
		}

	}

	void SwitchComputers()
	{
		openComputer.SetActive(!open);
		closedComputer.SetActive(open);
		open = !open;
	}

	public void DestinationButton()
	{
		slider.SetActive(false);
		// we should use the destination variable in generator to populate options
		//make sure to clear the options
		ClearOptions();
		for (int i = 0; i < generator.locations.Length; i++)
		{
			GameObject go = Instantiate(infoPrefab, displayPanel.transform);
			go.GetComponentInChildren<Text>().text = generator.locations[i].name;
			go.transform.GetChild(0).GetComponent<Image>().sprite = generator.locations[i].sprite;
			HandleDelegates(go.GetComponent<Tooltip>());

			string s = generator.locations[i].name;
			go.GetComponent<Tooltip>().message = s;
			go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

			int x = i;
			go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(0, x));

			options.Add(go);

			//destination = generator.locations[i];
			//playerData.SetDestination(generator.locations[i]);
		}
	}

	public void TransportationButton()
	{
		slider.SetActive(false);
		// we should use the destination variable in generator to populate options
		//make sure to clear the options
		ClearOptions();
		float travelTime = 0f;
		if (playerData.NPCHometown.landType == LandType.Island)
		{
			for(int i = 0 ; i < generator.transportOptions.Length ; i++)
			{
				if (generator.transportOptions[i].mode == Media.Air)
				{
					TravelOption to = new TravelOption();
					to.location = playerData.SuggestedDestination;
					to.transportationOption = new TransportationData();
					to.transportationOption.SetName(generator.transportOptions[i].name);
					to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
					to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
					to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
					to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
					travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
					GameObject go = Instantiate(infoPrefab, displayPanel.transform);
					go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

					HandleDelegates(go.GetComponent<Tooltip>());

					string s = generator.transportOptions[i].name;
					go.GetComponent<Tooltip>().message = s;
					go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

					int x = i;
					go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

					options.Add(go);
				}
				else if (generator.transportOptions[i].mode == Media.Water)
				{
					if ((playerData.SuggestedDestination.tags.Contains(LocationTags.Lake) ||
					playerData.SuggestedDestination.tags.Contains(LocationTags.Seaside)) &&
					(playerData.NPCHometown.tags.Contains(LocationTags.Lake) ||
					playerData.NPCHometown.tags.Contains(LocationTags.Seaside)))
					{
						TravelOption to = new TravelOption();
						to.location = playerData.SuggestedDestination;
						to.transportationOption = new TransportationData();
						to.transportationOption.SetName(generator.transportOptions[i].name);
						to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
						to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
						to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
						to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
						travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
						GameObject go = Instantiate(infoPrefab, displayPanel.transform);
						go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

						HandleDelegates(go.GetComponent<Tooltip>());

						string s = generator.transportOptions[i].name;
						go.GetComponent<Tooltip>().message = s;
						go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

						int x = i;
						go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

						options.Add(go);
					}
				}
			}
		}
		//it is continental
		else
		{
			for (int i = 0; i < generator.transportOptions.Length; i++)
			{
				if (playerData.NPCHometown.continent.Equals(playerData.SuggestedDestination.continent))
				{
					//if the continent is the same, allow ground travel
					if (generator.transportOptions[i].mode == Media.Land)
					{
						TravelOption to = new TravelOption();
						to.location = playerData.SuggestedDestination;
						to.transportationOption = new TransportationData();
						to.transportationOption.SetName(generator.transportOptions[i].name);
						to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
						to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
						to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
						to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
						travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
						//take care of accomodation
						//travelOptions[playerData.SuggestedDestination].Add(to);
						GameObject go = Instantiate(infoPrefab, displayPanel.transform);
						go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

						HandleDelegates(go.GetComponent<Tooltip>());

						string s = generator.transportOptions[i].name;
						go.GetComponent<Tooltip>().message = s;
						go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

						int x = i;
						go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

						options.Add(go);

					}
					else if (generator.transportOptions[i].mode == Media.Water)
					{
						//water options are available
						if (playerData.SuggestedDestination.tags.Contains(LocationTags.Lake) &&
						playerData.NPCHometown.tags.Contains(LocationTags.Lake))
						{
							TravelOption to = new TravelOption();
							to.location = playerData.SuggestedDestination;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(generator.transportOptions[i].name);
							to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
							to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
							to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
							to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
							travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
							GameObject go = Instantiate(infoPrefab, displayPanel.transform);
							go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

							HandleDelegates(go.GetComponent<Tooltip>());

							string s = generator.transportOptions[i].name;
							go.GetComponent<Tooltip>().message = s;
							go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

							int x = i;
							go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

							options.Add(go);
						}
					}
					else
					{
						TravelOption to = new TravelOption();
						to.location = playerData.SuggestedDestination;
						to.transportationOption = new TransportationData();
						to.transportationOption.SetName(generator.transportOptions[i].name);
						to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
						to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
						to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
						to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
						travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
						GameObject go = Instantiate(infoPrefab, displayPanel.transform);
						go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

						HandleDelegates(go.GetComponent<Tooltip>());

						string s = generator.transportOptions[i].name;
						go.GetComponent<Tooltip>().message = s;
						go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

						int x = i;
						go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

						options.Add(go);
					}
				}
				//the continents are not the same
				else
				{
					if (generator.transportOptions[i].mode == Media.Water)
					{
						//water options are available
						if ((playerData.SuggestedDestination.tags.Contains(LocationTags.Lake) ||
							playerData.SuggestedDestination.tags.Contains(LocationTags.Seaside)) &&
							(playerData.NPCHometown.tags.Contains(LocationTags.Lake) ||
							playerData.NPCHometown.tags.Contains(LocationTags.Seaside)))
						{

							TravelOption to = new TravelOption();
							to.location = playerData.SuggestedDestination;
							to.transportationOption = new TransportationData();
							to.transportationOption.SetName(generator.transportOptions[i].name);
							to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
							to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
							to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
							to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
							travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
							GameObject go = Instantiate(infoPrefab, displayPanel.transform);
							go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

							HandleDelegates(go.GetComponent<Tooltip>());

							string s = generator.transportOptions[i].name;
							go.GetComponent<Tooltip>().message = s;
							go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

							int x = i;
							go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

							options.Add(go);
						}
					}
					else if (generator.transportOptions[i].mode == Media.Air)
					{
						TravelOption to = new TravelOption();
						to.location = playerData.SuggestedDestination;
						to.transportationOption = new TransportationData();
						to.transportationOption.SetName(generator.transportOptions[i].name);
						to.transportationOption.SetSpeed(generator.transportOptions[i].Speed);
						to.transportationOption.SetMedia(generator.transportOptions[i].Mode);
						to.transportationOption.SetClass(generator.transportOptions[i].ClassType);
						to.transportationOption.SetCost(playerData.NPCHometown.Distance(playerData.SuggestedDestination) * generator.transportOptions[i].Cost);
						travelTime = playerData.NPCHometown.Distance(playerData.SuggestedDestination) / generator.transportOptions[i].Speed;
						GameObject go = Instantiate(infoPrefab, displayPanel.transform);
						go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

						HandleDelegates(go.GetComponent<Tooltip>());

						string s = generator.transportOptions[i].name;
						go.GetComponent<Tooltip>().message = s;
						go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

						int x = i;
						go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

						options.Add(go);
					}
				}


			}

		}
		//for (int i = 0; i < generator.transportOptions.Length; i++)
		//{
		//	GameObject go = Instantiate(infoPrefab, displayPanel.transform);
		//	go.GetComponentInChildren<Text>().text = generator.transportOptions[i].name;

		//	HandleDelegates(go.GetComponent<Tooltip>());

		//	string s = generator.transportOptions[i].name;
		//	go.GetComponent<Tooltip>().message = s;
		//	go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

		//	int x = i;
		//	go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(1, x));

		//	options.Add(go);
		//}
	}

	public void AccomodationButton()
	{
		slider.SetActive(false);
		ClearOptions();
		for (int i = 0; i < generator.accomodations.Length; i++)
		{
			GameObject go = Instantiate(infoPrefab, displayPanel.transform);
			go.GetComponentInChildren<Text>().text = generator.accomodations[i].name;

			HandleDelegates(go.GetComponent<Tooltip>());

			string s = string.Format("{0:C}", generator.accomodations[i].costPerNight) + " per night, "
				+ generator.accomodations[i].RoomType.ToString() + ", "
				+ generator.accomodations[i].Stars.ToString() + " Stars";
			go.GetComponent<Tooltip>().message = s;
			go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

			int x = i;
			go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(2, x));

			options.Add(go);
		}
	}

	void HandleDelegates(Tooltip tt)
	{
		ShowTooltip[] shows = FindObjectsOfType<ShowTooltip>();
		//Debug.Log(shows.Length);
		for (int x = 0; x < shows.Length; x++)
		{
			tt.PointerEntered -= shows[x].OnShowTip;
			tt.PointerExited -= shows[x].OnHideTip;
		}

		tt.PointerEntered += showTooltip.OnShowTip;
		tt.PointerExited += showTooltip.OnHideTip;
	}
	private void DialogChosen(int option, int index)
	{
		switch (option)
		{
			case 0:
				//Debug.Log(generator.locations[index].name);
				//destinationInfo.GetComponentInChildren<Text>().text = generator.locations[index].name;
				playerData.SetDestination(generator.locations[index]);
				if (destinationInfo == null)
				{
					destinationInfo = Instantiate(infoPrefab, horizontalBar.transform);
					Destroy(destinationInfo.GetComponent<Tooltip>());
					
				}
				else
				{
					if (transportationInfo != null)
					{
						Destroy(transportationInfo);
						transportationInfo = null;
						playerData.SetTransportation(null);
					}
				}
				destinationInfo.GetComponentInChildren<Text>().text = generator.locations[index].name;
				destinationInfo.transform.GetChild(0).GetComponent<Image>().sprite = generator.locations[index].sprite;

				SetButtons(1);

				break;
			case 1:
				//Debug.Log(generator.transportOptions[index].name);
				//transportationInfo.GetComponentInChildren<Text>().text = generator.transportOptions[index].name;
				playerData.SetTransportation(generator.transportOptions[index]);
				if (transportationInfo == null)
				{
					transportationInfo = Instantiate(infoPrefab, horizontalBar.transform);
					Destroy(transportationInfo.GetComponent<Tooltip>());
				}
				transportationInfo.GetComponentInChildren<Text>().text = generator.transportOptions[index].name;

				if (budgetInfo == null)
				{
					budgetInfo = Instantiate(infoBudgetPrefab, horizontalBar.transform);
					Destroy(budgetInfo.GetComponent<Tooltip>());
					budgetInfo.transform.GetChild(0).GetComponent<Image>().sprite = playerData.money;
				}

				budgetInfo.GetComponentInChildren<Text>().text = string.Format("{0:C}", playerData.CurrentBudget);
				SetButtons(2);
				break;
			case 2:
				//Debug.Log(generator.accomodations[index].name);
				if (accomodationInfo == null)
				{
					accomodationInfo = Instantiate(infoPrefab, horizontalBar.transform);
					Destroy(accomodationInfo.GetComponent<Tooltip>());
				}

				accomodationInfo.GetComponentInChildren<Text>().text = generator.accomodations[index].name;
				playerData.SetAccomodation(generator.accomodations[index]);

				SetButtons(3);
				break;
			case 3:
				//Debug.Log(generator.activities[index].name);
				//activityInfo.GetComponentInChildren<Text>().text = generator.activities[index].name;
				//add to the dictionary
				if (!activityInfo.ContainsKey(generator.activities[index].name))
				{
					GameObject go = Instantiate(infoPrefab, horizontalBar.transform);
					Destroy(go.GetComponent<Tooltip>());
					go.GetComponentInChildren<Text>().text = generator.activities[index].name;
					activityInfo.Add(generator.activities[index].name, go);
					go.GetComponent<Button>().onClick.AddListener(() =>RemoveActivity(go));
					playerData.AddActivity(generator.activities[index]);
				}
				break;
		}
	}

	private void RemoveActivity(GameObject go)
	{
		activityInfo.Remove(go.GetComponentInChildren<Text>().text);
		Destroy(go);
	}

	public void Submit()
	{
		//TravelOption to = new TravelOption();
		//AccomodationData ad = new AccomodationData()
		//to.availableAccomodation
		if (Submitted != null)
		{
			Submitted();
		}
	}

	void OnBudgetChanged(float d)
	{
		GetComponent<SoundManager>().BudgetUpdated();
		if (budgetInfo == null)
		{
			budgetInfo = Instantiate(infoBudgetPrefab, horizontalBar.transform);
			Destroy(budgetInfo.GetComponent<Tooltip>());
			budgetInfo.transform.GetChild(0).GetComponent<Image>().sprite = playerData.money;
			budgetInfo.transform.GetChild(1).GetComponent<Text>().text = string.Format("{0:$#,##0.00}", playerData.CurrentBudget);
			budgetInfo.transform.GetChild(2).GetComponent<Text>().text = string.Format("{0:$#,##0.00}", d);
		}
		else
		{
			Destroy(budgetInfo);
			budgetInfo = Instantiate(infoBudgetPrefab, horizontalBar.transform);
			Destroy(budgetInfo.GetComponent<Tooltip>());
			budgetInfo.transform.GetChild(0).GetComponent<Image>().sprite = playerData.money;
			budgetInfo.transform.GetChild(1).GetComponent<Text>().text = string.Format("{0:$#,##0.00}", playerData.CurrentBudget);
			budgetInfo.transform.GetChild(2).GetComponent<Text>().text = string.Format("{0:$#,##0.00}", d);
		}
		//GameObject go = Instantiate(budgetPopup);
		//go.transform.SetParent(budgetInfo.transform);
		//go.GetComponent<Text>().text = string.Format("{0:C}", d);
		//Debug.Log(d);
	}

	public void ActivityButton()
	{
		slider.SetActive(false);
		ClearOptions();

		for (int i = 0; i < generator.activities.Length; i++)
		{
			foreach (var tag in generator.activities[i].tags)
			{
				if (playerData.SuggestedDestination.tags.Contains(tag))
				{
					GameObject go = Instantiate(infoPrefab, displayPanel.transform);
					go.GetComponentInChildren<Text>().text = generator.activities[i].name;

					HandleDelegates(go.GetComponent<Tooltip>());

					string s = string.Format("{0:C}", generator.activities[i].cost) + ", "
						+ generator.activities[i].timeToDo + " minutes, "
						+ "O: " + generator.activities[i].openness +
						", C: " + generator.activities[i].conscientiousness +
						", E: " + generator.activities[i].extraversion +
						", A: " + generator.activities[i].agreeableness +
						", N: " + generator.activities[i].neuroticism;
					go.GetComponent<Tooltip>().message = s;
					go.GetComponent<Tooltip>().pos = go.GetComponent<RectTransform>().anchoredPosition;

					int x = i;
					go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(3, x));


					options.Add(go);
				}
			}
			
		}
	}

	public void LengthOfStayButton()
	{
		ClearOptions();
		slider.SetActive(true);
	}

	public void LengthOfStayChanged(float f)
	{
		if (lengthOfStayInfo == null)
		{
			lengthOfStayInfo = Instantiate(infoPrefab, horizontalBar.transform);
			Destroy(lengthOfStayInfo.GetComponent<Tooltip>());
		}
		lengthOfStayInfo.GetComponentInChildren<Text>().text = f.ToString() + " day(s)";
		playerData.SetLength(Mathf.FloorToInt(f));
	}

	void ClearOptions()
	{
		//avoid errors
		for (int i = options.Count - 1; i > -1; i--)
		{
			Destroy(options[i]);
		}

		options.Clear();
	}
}
