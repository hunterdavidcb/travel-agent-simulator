﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowThreeDTooltip : MonoBehaviour
{
	RectTransform tooltip;

	void Awake()
	{
		tooltip = transform.GetChild(0).GetComponent<RectTransform>();

		tooltip.gameObject.SetActive(false);

		//Tooltip[] tips = FindObjectsOfType<Tooltip>();
		////go through and subscribe;
		//for (int i = 0; i < tips.Length; i++)
		//{
		//	tips[i].ShowTip += OnShowTip;
		//	tips[i].HideTip += OnHideTip;
		//}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void OnHideTip(string m, Vector3 pos)
	{
		tooltip.position = Vector3.zero;
		tooltip.gameObject.SetActive(false);
	}

	public void OnShowTip(string m, Vector3 pos)
	{

		//pos = Camera.main.WorldToScreenPoint(pos);
		//Debug.Log(pos);
		//pos.y /= 2f;
		//pos.x /= 2f;
		pos.y += 45;
		//pos.x -= 40;
		tooltip.anchoredPosition = pos;
		tooltip.GetComponentInChildren<Text>().text = m;
		tooltip.gameObject.SetActive(true);

	}
}
