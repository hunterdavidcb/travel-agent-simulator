﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public delegate void PointerHandler(string s, Vector3 pos);
	public event PointerHandler PointerEntered;
	public event PointerHandler PointerExited;
	public string message = "";
	public Vector3 pos = Vector3.zero;

	public void OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log("local: " + GetComponent<RectTransform>().localPosition);
		//Debug.Log("anchored: " + GetComponent<RectTransform>().anchoredPosition);
		//Debug.Log(eventData.pointerCurrentRaycast.screenPosition);
		if (PointerEntered != null)
		{
			if (pos != Vector3.zero)
			{
				PointerEntered(message, eventData.pointerCurrentRaycast.screenPosition);
			}
			else
			{
				PointerEntered(message, GetComponent<RectTransform>().anchoredPosition);
			}
			
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		string s = "";
		Vector3 p = new Vector3();
		if (PointerExited != null)
		{
			PointerExited(s, p);
		}
	}

	// Start is called before the first frame update
	void Awake()
    {
		PointerEntered += FindObjectOfType<ShowTooltip>().OnShowTip;
		PointerExited += FindObjectOfType<ShowTooltip>().OnHideTip;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
