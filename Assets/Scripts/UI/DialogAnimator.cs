﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAnimator : MonoBehaviour
{
	public delegate void DialogAnimHandler(string s = "");
	public event DialogAnimHandler Shown;
	public event DialogAnimHandler Hidden;

	private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Show()
	{
		anim.Play("Show");
	}

	public void Hide()
	{
		anim.Play("Hide");
	}

	//use this to trigger the beginning of the textreveal animation
	public void OnShown()
	{
		//Debug.Log("Shown");
		if (Shown != null)
		{
			Shown();
		}
	}

	//use this to allow other options to be shown
	public void OnHidden()
	{
		//Debug.Log("Hidden");
		if (Hidden != null)
		{
			Hidden();
		}
	}
}
