﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjectOnGlobe : MonoBehaviour
{
	public GameObject globe;
	//public Location[] places;
	public List<Transform> cities = new List<Transform>();
	public GameObject cityPref;
	float R = 0.5f;

	// Start is called before the first frame update
	void Start()
	{
		
	}

	public void SpawnPoints(Location[] places)
	{
		for (int i = 0; i < places.Length; i++)
		{
			float x = R * Mathf.Cos(places[i].latLong.latitude * Mathf.Deg2Rad) *
			Mathf.Cos(places[i].latLong.longitude * Mathf.Deg2Rad);

			float y = R * Mathf.Sin(places[i].latLong.latitude * Mathf.Deg2Rad);

			float z = R * Mathf.Cos(places[i].latLong.latitude * Mathf.Deg2Rad) *
				Mathf.Sin(places[i].latLong.longitude * Mathf.Deg2Rad);
			Vector3 pos = globe.transform.position + new Vector3(x, y, z);
			GameObject go = Instantiate(cityPref, pos, Quaternion.identity);
			go.transform.SetParent(globe.transform);

			cities.Add(go.transform);

			go.GetComponent<ThreeDTooltip>().message = places[i].name;
		}
	}
}
