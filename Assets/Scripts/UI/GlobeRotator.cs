﻿using UnityEngine;
using System.Collections;

public class GlobeRotator : MonoBehaviour
{
	float horizontalRotSpeed = 40f;
	float verticalRotSpeed = 20f;
	//float rotY = 0f;
	float totalY = 0f;
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void OnMouseDrag()
	{
		float rotX = Input.GetAxis("Mouse X") * horizontalRotSpeed;// * Mathf.Deg2Rad;
		float rotY = Input.GetAxis("Mouse Y") * verticalRotSpeed;// * Mathf.Deg2Rad;
		

		if (totalY + rotY < -60f)
		{
			rotY = 0f;
		}
		else if (totalY +  rotY > 60f)
		{
			rotY = 0f;
		}

		totalY += rotY;



		Quaternion from = transform.rotation;
		Quaternion to = from * Quaternion.Euler(0f, -rotX, 0);
		transform.rotation = Quaternion.RotateTowards(from, to, horizontalRotSpeed);

		
		transform.RotateAround(transform.position, Vector3.forward, -rotY);
	}
}
