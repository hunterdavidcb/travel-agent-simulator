﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	public AudioClip button;
	public AudioClip money;
	public AudioClip airplane;
	public AudioClip bus;
	public AudioClip boat;
	public AudioClip train;

	AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
		source = GetComponent<AudioSource>();
		FindObjectOfType<DrawGreatCircle>().VehicleSpawned += OnVehicleSpawned;
    }

	private void OnVehicleSpawned(int i)
	{
		Debug.Log("yay");
		switch (i)
		{
			case 0:
				Train();
				break;
			case 1:
				Bus();
				break;
			case 2:
				Boat();
				break;
			case 3:
				Airplane();
				break;
			default:
				break;
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void ButtonPressed()
	{
		//Debug.Log("button");
		if (!source.isPlaying)
		{
			source.clip = button;
			source.Play();
		}
		
	}

	public void BudgetUpdated()
	{
		//Debug.Log("money");
		source.clip = money;
		source.Play();
	}

	void Airplane()
	{
		//Debug.Log("vehicle");
		source.clip = airplane;
		source.Play();
	}

	void Bus()
	{
		Debug.Log("vehicle");
		source.clip = bus;
		source.Play();
	}

	void Boat()
	{
		//Debug.Log("vehicle");
		source.clip = boat;
		source.Play();
	}

	void Train()
	{
		//Debug.Log("vehicle");
		source.clip = train;
		source.Play();
	}
}
