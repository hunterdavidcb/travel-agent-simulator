﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateOnSlide : MonoBehaviour
{
	Text t;
	public delegate void Updated(int a);
	public event Updated UpdatedStay;
    // Start is called before the first frame update
    void Start()
    {
		t = GetComponentInChildren<Text>();
		t.text = 0.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void SliderUpdated(float f)
	{
		if (t != null)
		{
			t.text = f.ToString();
		}

		if (UpdatedStay != null)
		{
			UpdatedStay(Mathf.FloorToInt(f));
		}
		
		
	}
}
