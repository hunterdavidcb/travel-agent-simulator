﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceButtonCallback : MonoBehaviour 
{
    public delegate void ButtonClickHandler(float waitTime, float intensity, float matchPercentage,string option);
    public event ButtonClickHandler ButtonClicked;
    // Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void Register(float waitTime, float intensity, float matchPercentage,string option)
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(() => OnButtonClicked(waitTime,intensity,matchPercentage,option));
    }

    public void Deregister()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.RemoveAllListeners();
    }

    private void OnButtonClicked(float waitTime, float intensity, float matchPercentage,string option)
    {
        //Debug.Log("clicked " + gameObject.GetComponentInChildren<Text>().text);
        if (ButtonClicked != null)
        {
            //Debug.Log("not null");
            ButtonClicked(waitTime, intensity, matchPercentage,option);
        }
    }
}
