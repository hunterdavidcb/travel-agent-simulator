﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace DialogSystem
{
	
	public class DialogBox : MonoBehaviour
	{
		public SoundManager soundManager;

		public GameObject dialogOptionPrefab;

		public DialogCategory main;

		private List<GameObject> options = new List<GameObject>();


		private List<DialogOption> currentOptions;

		private Stack<List<DialogOption>> previousOptions;

		private Stack<DialogOption> previousChoices;

		private Stack<DialogOption> copy = new Stack<DialogOption>();

		Image image;


		NPC current = null;

		bool ignore = false;
		TextRevealer textRevealer;
		DialogAnimator animator;

		private void Awake()
		{
			image = GetComponentInChildren<Image>();
			image.enabled = false;
			//DialogEventManager.DialogEntered += OnDialogEntered;
			//DialogEventManager.DialogLeft += OnDialogLeft;

			previousOptions = new Stack<List<DialogOption>>();
			//Debug.Log(dialogOptions.Count);


			previousChoices = new Stack<DialogOption>();

			//fix later
			//TestNew.Left += OnDialogLeft;

			textRevealer = FindObjectOfType<TextRevealer>();
			animator = FindObjectOfType<DialogAnimator>();

			animator.Shown += OnShown;
			animator.Hidden += OnHidden;
		}

		internal void Register(NPCHolder npcHolder)
		{
			npcHolder.NPCClicked += OnDialogEntered;
			//Debug.Log("registered");
		}

		protected void OnFinished()
		{
			ignore = false;
		}

		protected void OnStarted()
		{
			ignore = true;
		}



		private void OnDialogEntered(NPC npc)
		{
			if (!ignore)
			{
				//Debug.Log("yay! Callbacks");
				current = npc;
				animator.GetComponentInChildren<Button>().onClick.AddListener(React);
				current.NPCReacted += ShowNPCReaction;
				image.enabled = true;
				currentOptions = new List<DialogOption>();
				for (int i = 0; i < main.subOptions.Count; i++)
				{
					DialogOption dialogOption = new DialogOption();
					dialogOption.name = main.subOptions[i].name;
					dialogOption.say = main.subOptions[i].say;
					if (main.subOptions[i] is DialogProbe)
					{
						dialogOption.trait = ((DialogProbe)main.subOptions[i]).trait;
					}
					dialogOption.SetSuboptions(main.subOptions[i].subOptions);
					currentOptions.Add(dialogOption);
				}

				CreateOptions(currentOptions);
			}

		}

		private void DialogChosen(int index)
		{
			previousChoices.Push(currentOptions[index]);
			previousOptions.Push(currentOptions);

			//delete all the current game objects
			Clear();

			//Debug.Log(previousChoices.Peek().name);

			if (currentOptions[index].subOptions.Count > 0)
			{
				CreateOptions(currentOptions[index].subOptions);
				currentOptions = currentOptions[index].subOptions;
			}
			else
			{
				animator.Show();
				textRevealer.RestartWithText(currentOptions[index].say);
				textRevealer.allRevealed.AddListener(OnTextCompleted);
				textRevealer.GetComponentInChildren<Button>().interactable = false;
				copy.Clear();
				copy = new Stack<DialogOption>(previousChoices.Reverse());
				//Debug.Log(previousChoices.Peek().name);
				//Debug.Log(copy.Peek().name);
				//current.ReactTo(previousChoices);
				//these must be cleared because it will cause unusual triggers and artifacts later
				previousChoices.Clear();
				previousOptions.Clear();
				image.enabled = false;
			}



		}

		protected void OnShown(string s)
		{
			textRevealer.RevealNextParagraphAsync();
		}

		private void React()
		{
			current.ReactTo(copy);
		}

		private void OnTextCompleted()
		{
			textRevealer.GetComponentInChildren<Button>().interactable = true;
		}

		protected void ShowNPCReaction(string s)
		{
			//helps?
			//if (textRevealer.IsRevealing)
			//{
			//	textRevealer.StopAllCoroutines();
			//}

			textRevealer.GetComponentInChildren<Button>().interactable = false;
			textRevealer.RestartWithText(s);
			textRevealer.RevealNextParagraphAsync();
			animator.GetComponentInChildren<Button>().onClick.RemoveListener(React);
			animator.GetComponentInChildren<Button>().onClick.AddListener(animator.Hide);
		}

		protected void OnHidden(string s)
		{
			//animator.Show();
			
			animator.GetComponentInChildren<Button>().onClick.RemoveListener(animator.Hide);
		}

		private void CreateOptions(List<DialogOption> dialogs)
		{
			bool set = false;
			for (int i = 0; i < dialogs.Count; i++)
			{
				GameObject go = Instantiate(dialogOptionPrefab, transform);

				go.GetComponentInChildren<Text>().text = dialogs[i].name;
				options.Add(go);

				if (dialogs[i].name == "Exit")
				{
					set = true;
					go.GetComponent<Button>().onClick.AddListener(() => OnDialogLeft());
				}
				else
				{
					// to prevent weird out of index or ref does not exist errors
					int x = i;

					go.GetComponent<Button>().onClick.AddListener(() => DialogChosen(x));
				}

				go.GetComponent<Button>().onClick.AddListener(soundManager.ButtonPressed);
			}

			if (!set)
			{
				GameObject goTwo = Instantiate(dialogOptionPrefab, transform);
				goTwo.GetComponentInChildren<Text>().text = "Back";

				options.Add(goTwo);

				goTwo.GetComponent<Button>().onClick.AddListener(() => Return());
			}

		}

		private void Return()
		{
			Clear();

			if (previousChoices.Count > 0)
			{
				//label.text = previousChoices.Pop().name;
			}

			//Debug.Log(previousOptions.Count);

			if (previousOptions.Count > 0)
			{
				currentOptions = previousOptions.Pop();

				CreateOptions(currentOptions);

				UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);

			}
			else
			{
				OnDialogLeft();
			}
		}

		private void OnDialogLeft()
		{
			//Debug.Log("closing");
			current = null;
			image.enabled = false;
			Clear();
		}

		private void Clear()
		{
			for (int i = options.Count - 1; i > -1; i--)
			{
				if (options[i].GetComponentInChildren<Text>().text == "Exit")
				{
					options[i].GetComponent<Button>().onClick.RemoveAllListeners();
				}
				Destroy(options[i]);
			}
			options.Clear();
		}
	}
}
