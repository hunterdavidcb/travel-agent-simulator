﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThreeDTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Material mouseover;
	Material original;
	public delegate void PointerHandler(string s, Vector3 pos);
	public event PointerHandler PointerEntered;
	public event PointerHandler PointerExited;
	public string message = "";

	public void OnPointerEnter(PointerEventData eventData)
	{
		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
		pos.x -= Screen.width / 2f;
		pos.y -= Screen.height / 2f;

		GetComponent<Renderer>().material = mouseover;

		if (PointerEntered != null)
		{
			PointerEntered(message, pos);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		string s = "";
		Vector3 p = new Vector3();
		GetComponent<Renderer>().material = original;
		if (PointerExited != null)
		{
			PointerExited(s, p);
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		PointerEntered += FindObjectOfType<ShowThreeDTooltip>().OnShowTip;
		PointerExited += FindObjectOfType<ShowThreeDTooltip>().OnHideTip;
		original = GetComponent<Renderer>().material;
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
