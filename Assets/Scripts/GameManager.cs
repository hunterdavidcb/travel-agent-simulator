﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public Transform globe;
	public Transform computer;
	bool lookingAtGlobe = false;

	public delegate void ResetHandler();
	public event ResetHandler NPCReset;

	private void Awake()
	{
		GetComponent<PlayerData>().Finished += OnNPCFinished;
		globe.GetComponent<SpawnObjectOnGlobe>().SpawnPoints(GetComponent<TravelOptionsHolder>().locations);
		globe.GetComponent<DrawGreatCircle>().VehicleDestroyed += OnVehicleDestroyed;
	}

	private void OnVehicleDestroyed(int i)
	{
		StartCoroutine(LookAtComputer());
	}

	private void OnNPCFinished()
	{
		//first make sure we're looking at the globe
		StartCoroutine(LookAtGlobe());
	}

	// Start is called before the first frame update
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		//if (Input.GetKeyDown(KeyCode.G))
		//{
		//	if (!lookingAtGlobe)
		//	{
		//		StartCoroutine(LookAtGlobe());
		//	}
		//	else
		//	{
		//		StartCoroutine(LookAtComputer());
		//	}
			
		//}
	}

	IEnumerator LookAtGlobe()
	{
		//Vector3 targetRot = Quaternion.LookRotation(globe.transform.position - transform.position).eulerAngles;

		//Vector3 euler = transform.rotation.eulerAngles;
		float vel = 200f;
		float t = 0f;
		while (t < 1f)
		{
			Vector3 targetRot = Quaternion.LookRotation(globe.transform.position - transform.position).eulerAngles;

			Vector3 euler = transform.rotation.eulerAngles;
			//euler.x = Mathf.SmoothDampAngle(euler.x, targetRot.x, ref vel, 1f, 100f);
			euler.y = Mathf.SmoothDampAngle(euler.y, targetRot.y, ref vel, 0.25f, 250f);
			//euler.z = Mathf.SmoothDampAngle(euler.z, targetRot.z, ref vel, 1f, 100f);
			transform.rotation = Quaternion.Euler(euler);
			t += Time.deltaTime;
			yield return null;
		}

		lookingAtGlobe = true;
	}

	IEnumerator LookAtComputer()
	{
		//Vector3 targetRot = Quaternion.LookRotation(globe.transform.position - transform.position).eulerAngles;

		//Vector3 euler = transform.rotation.eulerAngles;
		float vel = 200f;
		float t = 0f;
		while (t < 1f)
		{
			Vector3 targetRot = Quaternion.LookRotation(computer.transform.position - transform.position).eulerAngles;

			Vector3 euler = transform.rotation.eulerAngles;
			//euler.x = Mathf.SmoothDampAngle(euler.x, targetRot.x, ref vel, 1f, 100f);
			euler.y = Mathf.SmoothDampAngle(euler.y, targetRot.y, ref vel, 0.25f, 250f);
			//euler.z = Mathf.SmoothDampAngle(euler.z, targetRot.z, ref vel, 1f, 100f);
			transform.rotation = Quaternion.Euler(euler);
			t += Time.deltaTime;
			yield return null;
		}

		lookingAtGlobe = false;

		if (NPCReset != null)
		{
			NPCReset();
		}
	}

}
